import React from 'react';
import { PATH_APP } from '../../routes/paths';
import EventIcon from '@material-ui/icons/Event';
// ----------------------------------------------------------------------

const ICONS = {

  eventIcon: <EventIcon />,
};

const navConfig = [
  {
    subheader: 'ทั่วไป',
    items: [
      {
        role: 2,
        title: 'Todos',
        href: PATH_APP.todos,
        icon: ICONS.eventIcon,
      }
    ]
  },
];

export default navConfig;
