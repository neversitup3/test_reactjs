import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { PATH_APP } from './../../../routes/paths';

LoginProtect.propTypes = {
  children: PropTypes.node
};

 function LoginProtect({ children, role }) {
    let token = localStorage.getItem("token");
    if(!token){
      return <Redirect to={PATH_APP.login} />;
    }


  return <>{children}</>;
}

export default LoginProtect;
