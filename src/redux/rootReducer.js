import { combineReducers } from 'redux';
import authReducer from './slices/auth';
import todoReducer from './slices/todo';

const rootReducer = combineReducers({
  auth: authReducer,
  todo: todoReducer
});

export { rootReducer };
