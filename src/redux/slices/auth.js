import { createSlice } from '@reduxjs/toolkit';
import { httpClient } from "../../utils/HttpClient";
import { server } from "../../constants";

const initialState = {
  isLoading: false,
  error: false,
  auth: []
};

const slice = createSlice({
  name: 'login',
  initialState,
  reducers: {
    startLoading(state) {
      state.isLoading = true;
    },
    hasError(state) {
      state.isLoading = false;
      state.error = true;
    },
    getAuthSuccess(state, action) {
      state.isLoading = false;
      state.auth = action.payload;
    },
    logout(state) {
      state.isLoading = false;
      state.error = true;
    }
  }
});

export default slice.reducer;

export function login (value, history) {
  return async (dispatch) => {
    try {
      dispatch(slice.actions.startLoading());
      const response = await httpClient.post(server.LOGIN_URL, value);
  
      if(response.status === 200) {
        const { token } = response.data;
        localStorage.setItem(server.TOKEN_KEY, token);
        dispatch(slice.actions.getAuthSuccess(response));
        history.push("/todos");
      }else{
          dispatch(slice.actions.hasError());
      }
    } catch(e) {
      dispatch(slice.actions.hasError());
    }
  }
}

export const logout = (history) => {
  return () => {
    localStorage.removeItem("token");
    history.push("/auth/login")
  };
};
