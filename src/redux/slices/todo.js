import { server } from "../../constants";
import { createSlice } from "@reduxjs/toolkit";
import { httpClient } from "../../utils/HttpClient";
// ----------------------------------------------------------------------

const initialState = {
  isLoading: false,
  isLoadingSuccess: false,
  error: false,
  todos: [],
  isStoreSuccess: false
};

const slice = createSlice({
  name: "todos",
  initialState,
  reducers: {
    startLoading(state) {
      state.isLoading = true;
    },
    getTodoSuccess(state, action) {
      state.isLoading = false;
      state.todos = action.payload;
    },
    hasError(state, action) {
      state.isLoading = false;
      state.error = action.payload;
    },
    storeTodoSuccess(state) {
      state.isLoading = false;
      state.isStoreSuccess = true;
      state.isLoadingSuccess = true;
    },
    initStoreSuccessToFalse(state) {
      state.isStoreSuccess = false;
      state.isLoadingSuccess = false;
    }
  }
});

export default slice.reducer;
export const { getHistory } = slice.actions;

export function getTodos() {
  return async (dispatch) => {
    try {
      dispatch(slice.actions.startLoading());
      const response = await httpClient.get(server.TODO_ALL);
      dispatch(slice.actions.getTodoSuccess(response.data));
    } catch (e) {
      dispatch(slice.actions.hasError(e));
    }
  };
}

export function storeTodos(todos) {
  return async (dispatch) => {
    try {
      dispatch(slice.actions.startLoading());
      const response = await httpClient.post(server.TODO_ALL, todos);
      dispatch(slice.actions.storeTodoSuccess(response.data));
    } catch (e) {
      dispatch(slice.actions.hasError(e));
    }
  };
}

export function updateTodos(todos) {
  return async (dispatch) => {
    try {
      dispatch(slice.actions.startLoading());
      const response = await httpClient.put(`${server.TODO_ALL}/${todos.id}`, todos);
      dispatch(slice.actions.storeTodoSuccess(response.data));
    } catch (e) {
      dispatch(slice.actions.hasError(e));
    }
  };
}

export function deleteTodos(todos) {
  return async (dispatch) => {
    try {
      dispatch(slice.actions.startLoading());
      const response = await httpClient.delete(`${server.TODO_ALL}/${todos.id}`);
      dispatch(slice.actions.storeTodoSuccess(response.data));
    } catch (e) {
      dispatch(slice.actions.hasError(e));
    }
  };
}

export function initcialTodoStatus() {
  return async (dispatch) => dispatch(slice.actions.initStoreSuccessToFalse());
}

