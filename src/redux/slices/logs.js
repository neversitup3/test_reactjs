import { server } from "./../../constants";
import { createSlice } from "@reduxjs/toolkit";
import { httpClient } from "./../../utils/HttpClient";

const initialState = {
  isLoading: false,
  isLoadingSuccess: false,
  error: false,
  logLists: [],
  countRows: 0,
};

const slice = createSlice({
  name: "logs",
  initialState,
  reducers: {
    startLoading(state) {
      state.isLoading = true;
    },
    timeoutAlert(state) {
      state.isLoadingSuccess = false;
    },
    hasError(state, action) {
      state.isLoading = false;
      state.error = action.payload;
    },
    getHistory(state, action) {
      state.isLoading = false;
      state.logLists = action.payload;
    },
    setCountRow(state, action) {
      state.countRows = action.payload;
    },
  },
});

export default slice.reducer;
export const { getHistory } = slice.actions;

export function getLogLists(page, keyword = null) {
  return async (dispatch) => {
    dispatch(slice.actions.startLoading());
    try {
      const response = await httpClient.get(
        `${server.LOGS}?page=${page}&keyword=${keyword}`
      );
      let customData = [];

      response.data.history.map((row, index) => {
        customData[index] = {
          id: row.logId,
          newFileName: row.newFileName,
          transcript: row.transcript,
          topic: row.topic,
          originalFileName: row.originalFileName,
          filePath: row.filePath,
          type: row.type,
          size: row.size,
          selfLink: row.selfLink,
          mediaLink: row.mediaLink,
          bucket: row.bucket,
          storageClass: row.storageClass,
          md5Hash: row.md5Hash,
          crc32c: row.crc32c,
          etag: row.etag,
          userInfo: row.userInfo,
          createdAt: row.createdAt,
        };
      });

      dispatch(slice.actions.getHistory(customData));
      dispatch(slice.actions.setCountRow(response.data.length));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}
