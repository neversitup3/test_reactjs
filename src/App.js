import './App.css';
import React, { useEffect } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import routes, { renderRoutes } from './../src/routes';
import { store } from './redux/store'
import { Provider } from 'react-redux'

function App() {
  const history = useHistory();
  const path = useLocation().pathname;

  useEffect(()=>{
    const token = localStorage.getItem("token");
    if(!token){
      history.push("/auth/login")
    }
  }, [])



  useEffect(()=>{
    const token = localStorage.getItem("token");

    if(token && path === "/auth/login"){history.push("/speech-to-text")};
    if(!token){
      history.push("/auth/login")
    }

  }, [path])

  return (
      <Provider store={store}>
        {renderRoutes(routes)}
      </Provider>
  );
}

export default App;
