import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Button from '@material-ui/core/Button';
import * as useTodos from "./../../redux/slices/todo";
import Dialogs from "./../components/todo/dialogs";
import Swal from 'sweetalert2';
import moment from "moment";

import {
  Grid,
  Card,
  CardContent,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: '100%',
    backgroundColor: theme.palette.background.paper,
    padding: 10,
    marginTop: 15,
    border: '0px solid',
    cursor: 'pointer'
  },
  outlineCard: {
    margin:'auto'
  },
  button: {
    margin: theme.spacing(1),
  },
  sectionTime: {
    textAlign: 'right'
  }
}));

const MyComponent = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const todo = useSelector(({ todo }) => todo);
  const [open, setOpen] = React.useState(false);
  const [title, setTitle] = React.useState(null);
  const [description, setDesciption] = React.useState(null);
  const [id, setId] = React.useState(null);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    clearState();
  };

  useEffect(() => {
    (async () => {
      if(!todo.isStoreSuccess) {
        await dispatch(useTodos.getTodos());
      }
      await dispatch(useTodos.initcialTodoStatus());
      setOpen(false);
      clearState();
    })();
  }, [dispatch, todo.isStoreSuccess]);

  useEffect(() => {

    if(todo.isLoadingSuccess) {
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'ดำเนินการสำเร็จ',
        showConfirmButton: false,
        timer: 1500
      })
    }
  }, [todo.isLoadingSuccess])

  const clearState = () => {
    setTitle(null);
    setDesciption(null);
    setId(null);
  }

  return (
    <>
    <Dialogs open={open} handleClose={handleClose} title={title} description={description} id={id}/>
    <Grid container spacing={3}>
      <Grid item xs={12} md={10} style={{ margin:'auto'}}>
        <Card style={{ backgroundColor: '#f5f5f5' }}>
          <Button
              variant="contained"
              color="primary"
              className={classes.button}
              onClick={handleClickOpen}
            >
            ADD
          </Button>
          <CardContent>
            {
              todo.todos.map((result) => {
                return  (
                  <List className={classes.root} onClick={() => {
                    setTitle(result.title);
                    setId(result._id);
                    setDesciption(result.description);
                    handleClickOpen();
                  }}>
                    <Grid container>
                      <Grid item xs={12} md={6}>
                        <h1>{result.title}</h1>
                      </Grid>
                      <Grid item xs={12} md={5} className={classes.sectionTime}>
                        <b>{moment(new Date(result.createdAt)).format('LL')}</b>
                      </Grid>
                    </Grid>
                    <Grid container>
                      <Grid item xs={12} md={12}>
                        { result.description }
                      </Grid>
                    </Grid>
                  </List>
                );
              })
            }
          </CardContent>
        </Card>
      </Grid>
    </Grid>
    </>
  );
};

export default MyComponent;
