import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import { useDispatch, useSelector } from "react-redux";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import DialogTitle from '@material-ui/core/DialogTitle';
import * as useTodos from "./../../../redux/slices/todo";
import DeleteIcon from '@material-ui/icons/Delete';
import Swal from 'sweetalert2';
import moment from "moment";
import {
    Grid
} from "@material-ui/core";


export default function ResponsiveDialog({ 
    open, 
    handleClose, 
    title, 
    description, 
    id 
}) {

  const initialTodos = {title, description, id};
  const [todo, setTodo] = useState(initialTodos);
  const dispatch = useDispatch();
  const submit = async () => {
    if(!todo.title || !todo.description) {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'กรุณากรอกข้อมูลให้ครบถ้วน!'
        })
    }else{
        if(!id) {
            await dispatch(useTodos.storeTodos(todo));
        }else{
            await dispatch(useTodos.updateTodos(todo));
        }
        setTodo(initialTodos);
    }
  };

  const DeleteButton = async () => {
    
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then(async(result) => {
        if (result.isConfirmed) {
            await dispatch(useTodos.deleteTodos(todo));
        }
    })
    setTodo(initialTodos);
  };

  useEffect(() => {
    setTodo(initialTodos);
  }, [id]);

  return (
    <div>
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
            MANAGE TODOS
            <br />
            {moment(new Date()).format('LL')}
        </DialogTitle>
        <DialogContent dividers>
        <Grid container spacing={2}>
            <Grid item xs={12} sm={12}>
                <TextField
                    fullWidth
                    label="Title"
                    value={todo.title}
                    onChange={(e) => setTodo({...todo, title: e.target.value})}
                />
            </Grid>
            <Grid item xs={12} sm={12}>
                <TextField
                    fullWidth
                    label="Description"
                    value={todo.description}
                    id="outlined-multiline-static"
                    multiline
                    rows={4}
                    variant="outlined"
                    onChange={(e) => setTodo({...todo, description: e.target.value})}
                />
            </Grid>
        </Grid>
        </DialogContent>
        <DialogActions>
          {
            id && <Button autoFocus onClick={handleClose} color="danger" onClick={DeleteButton} style={{ color: 'red'}}>
             <DeleteIcon />
            Delete
          </Button> }
          <Button autoFocus onClick={handleClose} color="primary" onClick={submit}>
            Save changes
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
