import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import React, { useState, useEffect } from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import "firebase/messaging";
import { DataGrid } from "@material-ui/data-grid";
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import PropTypes from "prop-types";
import SwipeableViews from "react-swipeable-views";
import VisibilityIcon from "@material-ui/icons/Visibility";
import * as useLogs from "./../../redux/slices/logs";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import CloseIcon from "@material-ui/icons/Close";
import Alert from '@material-ui/lab/Alert';
import CheckIcon from '@material-ui/icons/Check';
import Box from "@material-ui/core/Box";
import moment from "moment";

import {
  Grid,
  Card,
  TextField,
  CardContent,
  Button,
  TableContainer,
  Paper,
  Typography,
  IconButton,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "80%",
    marginTop: 100,
  },
  field: {
    marginTop: 16,
  },
  card: {
    padding: 20,
  },
  break: {
    flexBasis: "100%",
    height: 0,
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
  table: {
    minWidth: 650,
  },
  typography: {
    padding: theme.spacing(2),
  }
}));

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}

const MyComponent = () => {
  const history = useHistory();
  const classes = useStyles();
  const theme = useTheme();

  const [page, setPage] = useState(0);
  const [keyword, setKeyword] = useState(null);
  const [value, setValue] = React.useState(0);
  const [open, setOpen] = React.useState(false);
  const [name, setName] = React.useState("-");
  const [time, setTime] = React.useState("-");
  const [labelFile, setLabelFile] = React.useState("-");
  const [topic, setTopic] = React.useState("-");
  const [videoName, setVideoName] = React.useState("-");
  const [newFileName, setNewFileName] = React.useState("-");
  const [originalFileName, setOriginalFileName] = React.useState("-");
  const [type, setType] = React.useState("-");
  const [size, setSize] = React.useState("-");
  const [selfLink, setSelfLink] = React.useState("-");
  const [transcript, setTranscript] = React.useState("-");
  const [data, setData] = React.useState({
    videoName: "",
    file: null,
  });
  const [loading, setLoading] = React.useState(false);
  const logs = useSelector(({ logs }) => logs);
  const dispatch = useDispatch();

  useEffect(() => {
    (async () => {
      setLoading(true);
      await dispatch(useLogs.getLogLists(page, keyword));
      setLoading(false);
    })();
  }, [page, keyword]);


  const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
      <MuiDialogTitle disableTypography className={classes.root} {...other}>
        <Typography variant="h6">{children}</Typography>
        {onClose ? (
          <IconButton
            aria-label="close"
            className={classes.closeButton}
            onClick={onClose}
            style={{ fontSize: '3rem !important' , color: 'red' }}
          >
            <CloseIcon />
          </IconButton>
        ) : null}
      </MuiDialogTitle>
    );
  });

  const DialogContent = withStyles((theme) => ({
    root: {
      padding: theme.spacing(2),
    },
  }))(MuiDialogContent);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const chooseVideo = (e) => {
    let file = e.target.files[0];
    setLabelFile(file.name);

    if (e.target.files[0]) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = function (e) {
        // setPurchaseConfirm([reader.result]);
      };
      e.target.value = null;
      setData({ ...data, file });
    }
  };

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const handlePageChange = (params) => {
    setPage(params.page);
  };

  const renderButton = (params) => {
    let row = params.row;
    let name = row.userInfo[0]
      ? `${row.userInfo[0].name} ${row.userInfo[0].sureName}`
      : "-";
    let time = `${moment(row.createdAt).format("DD/MM/YYYY")} ${moment(
      row.createdAt
    ).format("HH:mm น.")}`;
    let topic = row.topic;
    let transcript = row.transcript;
    let videoName = row.topic;
    let newFileName = row.newFileName;
    let originalFileName = row.originalFileName;
    let type = row.type;
    let size = row.size;
    let selfLink = row.selfLink;

    return (
      <>
        <div style={{ display: "flex" }}>
          <IconButton
            aria-label="update"
            className={classes.margin}
            onClick={() => {
              setName(name);
              setTime(time);
              setTopic(topic);
              setTranscript(transcript);
              setVideoName(videoName);
              setNewFileName(newFileName);
              setOriginalFileName(originalFileName);
              setType(type);
              setSize(size);
              setSelfLink(selfLink);
              handleClickOpen();
            }}
          >
            <VisibilityIcon
              fontSize="small"
              style={{ color: "rgb(255, 152, 0)" }}
            />
          </IconButton>
        </div>
      </>
    );
  };

  const getUser = (params) => {
    let row = params.row;
    return row.userInfo[0]
      ? `${row.userInfo[0].name} ${row.userInfo[0].sureName}`
      : "-";
  };

  const createDate = (params) => {
    let row = params.row;
    return (
      <>
        <div>
          {moment(row.createdAt).format("DD/MM/YYYY")}{" "}
          {moment(row.createdAt).format("HH:mm น.")}
        </div>
      </>
    );
  };

  const columns = [
    {
      field: "createdAt",
      renderCell: createDate,
      headerName: "วันทีและเวลา",
      resizable: false,
      width: 180,
    },
    {
      field: "topic",
      headerName: "ชื่อวีดีโอ",
      resizable: false,
      width: 180,
    },
    {
      field: "size",
      headerName: "ขนาดไฟล์",
      resizable: false,
      width: 180,
    },
    {
      field: "type",
      headerName: "ชนิดไฟล์",
      resizable: false,
      width: 180,
    },
    {
      field: "userInfo",
      renderCell: getUser,
      headerName: "ชื่อผู้อัพโหลด",
      resizable: false,
      width: 180,
    },
    {
      field: "action",
      headerName: "ดูผลลัพธ์",
      renderCell: renderButton,
      editable: false,
      width: 150,
      type: "number",
    },
  ];
  return (
    <Grid container spacing={3}>
      <Grid item xs={12} md={8}>
        <Card>
          <CardContent>
            <TableContainer component={Paper} style={{ marginTop: 30 }}>
              <div style={{ height: "90vh", width: "100%" }}>
                <DataGrid
                  rows={logs.logLists}
                  columns={columns}
                  rowHeight={70}
                  pagination
                  rowCount={logs.countRows}
                  paginationMode="server"
                  onPageChange={handlePageChange}
                  loading={loading}
                  pageSize={10}
                />
              </div>
            </TableContainer>
          </CardContent>
        </Card>
      </Grid>

      <Grid item xs={12} md={4}>
        <Card>
          <CardContent>
            <div style={{ width: "100%" }}>
              <AppBar position="static" color="default">
                <Tabs
                  value={value}
                  onChange={handleChangeTab}
                  indicatorColor="primary"
                  textColor="primary"
                  variant="fullWidth"
                  aria-label="full width tabs example"
                >
                  <Tab label="ค้นหา" {...a11yProps(0)} />
                </Tabs>
              </AppBar>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={value}
                onChangeIndex={handleChangeIndex}
              >
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      fullWidth
                      label="ชื่อวีดีโอ"
                      onChange={(e) => setKeyword(e.target.value)}
                      value={keyword}
                    />
                  </Grid>
                </TabPanel>
              </SwipeableViews>
            </div>
          </CardContent>
        </Card>

        <Dialog
          fullScreen
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={open}
          style={{ width: '70%', margin: 'auto', marginRight: 0}}
        >
          <DialogTitle id="customized-dialog-title" onClose={handleClose}>
            รายละเอียด
          </DialogTitle>
          <DialogContent dividers>
            <TableContainer component={Paper} style={{ width: '70%' }}>
              <Table className={classes.table} aria-label="simple table" >
                <TableHead>
                  <TableRow>
                    <TableCell style={{ width: '40%' }}>วันที่ และ เวลา </TableCell>
                    <TableCell align="left" style={{ width: '60%' }}>{time}</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow >
                    <TableCell component="th" scope="row">ชื่อหัวข้ออัพโหลด</TableCell>
                    <TableCell align="left">{topic}</TableCell>
                  </TableRow>
                  <TableRow >
                    <TableCell component="th" scope="row">ชื่อไฟล์วีดีโอ</TableCell>
                    <TableCell align="left">{newFileName}</TableCell>
               `   </TableRow>
                  <TableRow >
                    <TableCell component="th" scope="row">ชื่อไฟล์วีดีโอก่อนอัพโหลด</TableCell>
                    <TableCell align="left">{originalFileName}</TableCell>
                  </TableRow>
                  <TableRow >
                    <TableCell component="th" scope="row">ประเภทไฟล์</TableCell>
                    <TableCell align="left">{type}</TableCell>
                  </TableRow>
                  <TableRow >
                    <TableCell component="th" scope="row">ขนาดไฟล์</TableCell>
                    <TableCell align="left">{size} KB</TableCell>
                  </TableRow>
                  <TableRow >
                    <TableCell component="th" scope="row">ลิงค์</TableCell>
                    <TableCell align="left">{selfLink}</TableCell>
                  </TableRow>
                  <TableRow >
                    <TableCell component="th" scope="row">ชื่อผู้อัพโหลด</TableCell>
                    <TableCell align="left">{name}</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
            <hr />
            <Typography gutterBottom> <span style={{ marginTop: 20,fontSize: 20, lineHeight: 2, fontWeight: '300px !important'}}> {transcript} </span></Typography>
          </DialogContent>
        </Dialog>
      </Grid>
    </Grid>
  );
};

export default MyComponent;
