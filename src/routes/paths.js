function path(root, sublink) {
  return `${root}${sublink}`;
}
const ROOTS = {
  main:"",
  auth:"/auth"
};

export const PATH_APP = {
  todos: path(ROOTS.main, '/todos'),
  login: path(ROOTS.auth, '/login')
};
