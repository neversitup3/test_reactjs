import React, { lazy } from 'react';
import { PATH_APP } from './paths';
import Layouts from '../Layouts/Dashboard';
import LoginProtect from './../components/Auth/loginProtect';

const HomeRoutes = {
  path: '*',
  guard: LoginProtect,
  layout: Layouts,
  routes: [
    {
      exact: true,
      guard: LoginProtect,
      path: "/",
      component: lazy(() => import('../views/todo'))
    },
    {
      exact: true,
      guard: LoginProtect,
      path: PATH_APP.todos,
      component: lazy(() => import('../views/todo'))
    },
  ]
};

export default HomeRoutes;
