export const apiUrl = "https://candidate.neversitup.com/todo";
export const imageUrl = "https://speech.topnews.co.th";


export const NETWORK_CONNECTION_MESSAGE =
  "Cannot connect to server, Please try again.";
export const NETWORK_TIMEOUT_MESSAGE =
  "A network timeout has occurred, Please try again.";
export const NOT_CONNECT_NETWORK = "NOT_CONNECT_NETWORK";


export const server = {
  LOGIN_URL: "users/auth",
  TODO_ALL: "todos",
  TOKEN_KEY: "token",
  UPLOADS: ""
};
